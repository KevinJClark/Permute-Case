<#
The word "cat" can be case-permuted in 8 ways.
The 8 ways can be thought of as a 3 bit binary integer.
A "1" represents a capital letter, and a "0" represents a lowercase letter.

cat -- 000
caT -- 001
cAt -- 010
cAT -- 011
Cat -- 100
CaT -- 101
CAt -- 110
CAT -- 111

In general, a word has 2^n case-permutations where n = number of letters in the word.
#>

Function Permute-Case
{
    For($i=0; $i -lt $args.Length; $i++) #Loop on each argument
    {
        $PermuteThis = $args[$i].ToLower() #Change argument to lowercase
        $WordLength = $PermuteThis.Length  #Measure word length
        $Combinations = [math]::Pow(2, $WordLength) #Calculate number of possible permutations
        For($j=0; $j -lt $Combinations; $j++) #Loop on each permutation of an argument
        {
            $BinaryCounter = $j #Copy the total "binary value" of the permutation into a variable we can subtract from
            $Permutation = "" #Set our permutation to empty string.  We will append a letter at a time.
            $LetterNumber=$WordLength-1 #Set a variable to represent "n" in 2^n's place for binary counting
            For($k=0; $k -lt $WordLength; $k++) #Loop on each letter of a permutation
            {
                $NextLetter = $PermuteThis.Substring($k, 1) #Grab the next letter from our base word
                If($BinaryCounter - [math]::Pow(2, $LetterNumber) -ge 0) #If the letter is a binary 1...
                {
                    $BinaryCounter = $BinaryCounter - [math]::Pow(2, $LetterNumber) #Subtract from binary total
                    $NextLetter = $NextLetter.ToUpper() #Change next letter to upper case
                }
                $Permutation = $Permutation.PadRight($k+1, $NextLetter) #Append next letter to permutation
                $LetterNumber-- #Shift to the right for the next letter
            }
            $Permutation #Print Permutation
        }
    }
}

Permute-Case Linux gt Windows